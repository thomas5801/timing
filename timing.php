<?php 
include 'config.php';

    if(cek_session($url."api/v1/check") === false){
        header('Location: logout.php');
        exit;
    }


    // ADD
    if(isset($_POST['event'])  AND isset($_POST['prefix'])){
        
        $event_timing_tag = $_POST['event'];
        $prefix_timing_tag = $_POST['prefix'];
        

        $hashed_string['titaEvnhId'] = $event_timing_tag;
        $hashed_string['titaPrefix'] = $prefix_timing_tag;
        
        $data_post = array(
            'data' => $hashed_string,
        );
        
        $response = get_content($url.'/api/v1/timingtag/', json_encode($data_post));
        
        unset($_POST['event']);
        unset($_POST['prefix']);
        

		
		
        $response = json_decode($response);

       if(isset($response->status->error->message)){
           echo "<font color='red'><b> ERROR : ". $response->status->error->message."</b></font>";
           
           echo "<br>";
           echo "<br>";
       }else{
           header('Location: ' ."?cari_aja=".$response->data->links->titaEvnhId);
       }
    }
	
	

	if(isset($_POST['id_update'])  AND isset($_POST['prefix_update'])){
        
        $id_update = $_POST['id_update'];
        $prefix_update = $_POST['prefix_update'];
        

        $hashed_string['titaPrefix'] = $prefix_update;
        
        $data_post = array(
            'data' => $hashed_string,
        );
        
		
        $response = get_content($url.'/api/v1/update_timingtag/'.$id_update, json_encode($data_post));
        
		
		
        unset($_POST['id_update']);
        unset($_POST['prefix_update']);
        
		

        $response = json_decode($response);
		
		
		

       if(isset($response->status->error->message)){
           echo "<font color='red'><b> ERROR : ". $response->status->error->message."</b></font>";
           
           echo "<br>";
           echo "<br>";
       }else{
           header('Location: ' ."?cari_aja=".$response->data->links->titaEvnhId);
       }
    }

   
?>
<style>


.back {
  display: block;
  width: 100%;
  border: true;
  background-color: #4CAF50;
  color: white;
  padding: 10px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block {
  display: block;
  width: 100%;
  border: true;
  background-color: #BA55D3;
  color: white;
  padding: 3px 60px;
  font-size: 12px;
  cursor: pointer;
  text-align: center;
}

.block:hover {
  background-color: #D8BFD8;
  color: black;
}


ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #A9A9A9;
  width:600;
}

li {
  float: left;
   width:75;
}



li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #ddd;
  color: black;
}

.active {
  background-color: #800080;
  color: white;;
}


</style>


<ul >
  <li><a class="active" href="timing.php">Timing</a></li>
  <li><a href="logout.php">Logout</a></li>
</ul>

<br>

<table border="0" width='600'>
    <tr>
        <td><h3> Form Tambah Prefix Timing  </h3></td>

    <tr>
    
</table>


<?php

    if(isset($_GET['cari'])){
        $cari = $_GET['cari'];
    }
	if(isset($_GET['cari_aja'])){
        $cari_aja = $_GET['cari_aja'];
    }

    //error_reporting(0);

    // GET DATA
    $ch = curl_init(); 
    
    if(isset($cari)){
        $url_ = $url."api/v1/resources/timing_tag/".$cari; 
    }else{
        $url_ = $url."api/v1/resources/timing_tag/"; 
    }
	
	if(isset($cari_aja)){
		if(empty($cari_aja)){
			$url_ = $url."api/v1/resources/timing_tag/"; 
		}else{
			$url_ = $url."api/v1/resources/timing_tag/?filter[titaEvnhId]=".$cari_aja; 
		}
        
    }


	// set url
	curl_setopt($ch, CURLOPT_URL, $url_);

	// return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// tutup curl 
	curl_close($ch);      

	// menampilkan hasil curl
	echo " \n ";

	echo " \n ";
    $data_all = json_decode($output);
	
	
	if(!empty($cari)){
		 if(isset($data_all->data)){
			$update_id    = $data_all->data->id;
			$update_event =	$data_all->linked->titaEvnhId[0]->evnhName;

			$update_prefix = $data_all->data->titaPrefix;
			
			echo "<form action='timing.php' method='post'>
				
				<table width='600' cellpadding='0' cellspacing='2' bordercolor='#666666'>
					<tr>
						<th  colspan='2'>Update Timing Tag</th>
					</tr>
					<tr>
						<th  colspan='2'>&nbsp;</th>
					</tr>
					<tr>
						<td width='100'><b>&nbsp;&nbsp;</b></td>
						<td><input  value ='$update_id' type='number' min='1' name='id_update' required hidden></td>
					</tr>
					<tr>
						<td width='100'><b>&nbsp;&nbsp;Event Name</b></td>
						<td><input size='60' value ='$update_event'  type='text' name='event' required disabled></td>
					</tr>
					<tr>
						<td valign=top><b>&nbsp;&nbsp;Prefix</b></td>
						<td ><input value ='$update_prefix' size='60' type='text' name='prefix_update' rows=4 cols=40 required></textarea></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>
							<b><input type='submit' value='Kirim'></b>
						</td>
					</tr>
					<tr>
						<th  colspan='2'>&nbsp;</th>
					</tr>
				</table>
			</form>";

			
		 }else{
			header('Location: timing.php?error= Yang di cari ga ada');
			exit;
		 }
	 
	}else{
		
		echo "<form action='timing.php' method='post'>
			<table width='600' cellpadding='0' cellspacing='2' bordercolor='#666666'>
				<tr>
					<td width='100'><b>Event Id</b></td>
					<td><input  type='number' min='1' name='event' required ></td>
				</tr>
				<tr>
					<td valign=top><b>Prefix</b></td>
					<td ><input  size='60' type='text' name='prefix' rows=4 cols=40 required></textarea></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<b><input type='submit' value='Submit'></b>
					</td>
				</tr>
			</table>
		</form>";
		
	}
	
	
	

?>



<br>

<table width='600'>
<tr>
<td>
<?php
if(isset($_GET['cari'])){
	$back = 'onclick="window.location.href='."'".'timing.php'."'".'"';
	echo "<form><button $back class ='block'>Kembali</button> </form>";
}


?>

</td>
<td align="right" valign="middle">
<form action="timing.php" method="get">
	<label>Cari :</label>
	<input type="text" name="cari_aja">
	<input type="submit" value="Cari">
</form>
</td>
</tr>

</table>

<?php

    if(isset($cari_aja)){
		echo "<b>Hasil pencarian : ".$cari_aja."</b>";   
    }
	
	if(isset($_GET['error'])){
		$error = urldecode($_GET['error']);
        echo "<b>Hasil pencarian : ".$error."</b><br><br>";   
    }
	
if(!isset($cari)){
		echo "<table width='600' border='1'>";
		echo "<tr><th style='background-color:#8B008B;color:#FFFFFF;' colspan='3' >=================== TIMING TAG ===================</th></tr>";
            

		if(isset($data_all->data)){
		
			$x = 1;
            foreach($data_all->data as $vall ){
            
                if($x % 2 == 0){
                     $style = "";
                }else{
                     $style = "style='background-color:#ddd;'";
                }
               
                
                echo "<tr $style>";
                echo "<td><b>Id</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->id."</td>";
                echo "</tr>";
                
                echo "<tr $style>";
                echo "<td><b>Event</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->links->titaEvnhId." </td>";
                echo "</tr>";
                
                
                $update  = 'onclick="window.location.href='."'".'timing.php?cari='.$vall->id."'".'"';
				$unduh	 = 'onclick="window.open('."'".$url."api/v1/datatiming/".$vall->links->titaEvnhId."')".'"';
                
                
                foreach($data_all->linked->titaEvnhId as $vall_event ){
                    if($vall_event->id == $vall->links->titaEvnhId){
                        $vall->links->titaEvnhId = $vall_event->evnhName;
                        break;
                    }
                    
                
                }
                echo "<tr $style>";
                echo "<td><b>Event Name</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->links->titaEvnhId." </td>";
                echo "</tr>";
                
                echo "<tr $style>";
                echo "<td><b>Prefix</b></td>";
                echo "<td>:</td>";
                echo "<td>".$vall->titaPrefix." </td>";
                echo "</tr>";
				
				
				
				
			
				
				$table_user ="
                        <table width=100%>
                        <tr>
                        
                        <td><button $update class='block'> Ubah  </button></td>
                        <td><button $unduh class='block'> Unduh Data</button></td>

                        </tr>
                        </table>";
						
				echo "<tr $style>";
				echo "<td colspan='3'> $table_user </td>";
				echo "</tr>";
									
                echo "<tr >";
                echo "<td colspan='3'>&nbsp;</td>";
                echo "</tr>";
                
                $x++;
            
            }
      
        
		}
		echo '</table>';
        
        if(!isset($data_all->status->totalRecords)){
			$data_all->status->totalRecords = 0;
		}
        echo "<table width='600' border='1'>";
        echo "<tr>";
        echo "<td><b>Total : ".$data_all->status->totalRecords."</b></td>";
 
        echo "</tr>";
        echo "</table>";

	}
    
   
    

?>

